#!/bin/bash
# shellcheck disable=SC2312         # masking of return values

eval "$(shellspec - -c) exit 1"

Describe 'jq'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'is new enough'
        It 'returns at least version 1.6 when asked'
            When call jq --version
            The output should not match pattern "jq-1.[012345]"
        End
    End
End

Describe 'ts'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'is new enough'
        It 'has support for -s'
            When call ts -s
            The status should be success
        End
    End
End
