#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

cd "$(dirname "${BASH_SOURCE[0]}")"

function fold_start {
    echo -e "\e[0Ksection_start:$(date +%s || true):$1[collapsed=true]\r\e[0K$2"
}

function fold_end {
    echo -e "\e[0Ksection_end:$(date +%s || true):$1\r\e[0K"
}

exit_code=0

for i in tests/test_*.sh; do
    fold_start "${i##*/}" "Running ${i}"
    "${i}" &
    wait $! && s=0 || s=$?
    fold_end "${i##*/}"
    if (( s > 0 )); then
        echo "  ^ the tests above exited with an error, expand the section for details"
        exit_code=1
    fi
done

exit "${exit_code}"
