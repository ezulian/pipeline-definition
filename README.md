# pipeline-definition

YAMLs for CKI pipeline definition

# Pipeline linting and unit tests

There is a `cki_tools.gitlab_yaml_shellcheck` linter script in [cki-tools] that
allows to lint the shell code embedded in the pipeline. This script is also
used in the GitLab CI/CD pipeline.

To run the linting and testing locally, use

```bash
podman run --pull=newer --rm --volume .:/data:Z --workdir /data \
    quay.io/cki/cki-tools:production \
    ./lint.sh
podman run --pull=newer --rm --volume .:/data:Z --workdir /data \
    quay.io/cki/builder-rawhide:production \
    ./tests.sh

# To run a subset of shellspec tests, e.g. within `Describe 'is_debug_build'`
podman run --pull=newer --rm --volume .:/data:Z --workdir /data -it \
    quay.io/cki/cki-tools:production \
    shellspec spec/functions_general_spec.sh --example "is_debug_build*"
```

In GitLab CI/CD, the tests run on all builder container images. This can be
simulated locally by replacing `quay.io/cki/builder-rawhide` with the
appropriate builder image.

# Pipeline testing

The preferred way is to make an MR and ask `cki-ci-bot` to test the changes. It
is also possible to use `retrigger` from [cki-tools]. This is what `cki-bot`
uses on the background too. You need to have valid tokens to use this method.

See the documentation on [pipeline triggering] for more details.

[cki-tools]: https://gitlab.com/cki-project/cki-tools
[pipeline triggering]: https://cki-project.org/l/pipeline-triggering/
