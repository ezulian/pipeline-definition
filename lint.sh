#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck disable=SC1091
. cki_utils.sh

declare -i FAILED=0

# The following checks are missing from --enable=all at the moment:
# - shellcheck 0.7.0:
#   add-default-case
#   check-unassigned-uppercase
# - shellcheck 0.8.0:
#   check-extra-masked-returns
#   check-set-e-suppressed
#   deprecate-which
#   require-double-brackets
shellcheck_options=(
    '--enable=avoid-nullary-conditions'
    '--enable=quote-safe-variables'
    '--enable=require-variable-braces'
)

cki_echo_yellow "Checking cki_pipeline.yml"
if ! python3 -m cki_tools.gitlab_yaml_shellcheck "${shellcheck_options[@]}" --check-sourced cki_pipeline.yml; then
    FAILED+=1
fi

for file in .gitlab-ci.yml cki_pipeline.yml; do
    cki_echo_yellow "Checking ${file}"
    if ! yamllint -s "${file}"; then
        FAILED+=1
    fi
done

if (( FAILED > 0 )); then
    cki_echo_red "${FAILED} linting steps failed."
    exit 1
fi
